package steps;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import utils.DriverFactory;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by kasbi on 17.07.2017.
 */
public class StartingStep extends DriverFactory{
    static Logger logger = Logger.getLogger(StartingStep.class.getName());

    @Before
    public void setup(){
        driver.manage().window().maximize();
        driver.get("http://www.bbc.co.uk/radio");
//        PropertyConfigurator.configure("log4j.properties");

        logger.info("Setup was run.");
        logger.warning("Setup was run.");
        logger.log(Level.OFF, "Setup was run.");
    }

    @After
    public void cleanup(){
        destroyDriver();
    }
}
