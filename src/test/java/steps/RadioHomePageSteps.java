package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.RadioHomePage;
import utils.DriverFactory;

import java.util.List;

/**
 * Created by kasbi on 17.07.2017.
 */
public class RadioHomePageSteps extends DriverFactory{

    @Given("I am on the homepage ([^\"]*)")
    public void iAmOnTheHomepage(String homepageAdress){
        new RadioHomePage(driver).iAmOnTheHomepage(homepageAdress);
    }

    @Given("I can see the radio nav")
    public void iCanSeeTheRadioNav() {
        new RadioHomePage(driver).iCanSeeTheRadioNav();
    }


    @When("^I select ([^\"]*) in the radio nav$")
    public void iSelectSectionsInTheRadioNav(String section){
        new RadioHomePage(driver).iSelectSectionsInTheRadioNav(section.toLowerCase());
    }

    @When("^I select ([^\"]*) in the radio nav again$")
    public void iSelectSectionsInTheRadioNavAgain(String section){
        new RadioHomePage(driver).iSelectSectionsInTheRadioNavAgain(section.toLowerCase());
    }

    @When("^I can see the Stations section content$")
    public void iCanSeeTheStationsSectionContent(){
        new RadioHomePage(driver).iCanSeeTheStationsSectionContent();
    }

    @And("^I select the all categories link$")
    public void iSelectTheAllCategoriesLink(){
        new RadioHomePage(driver).iSelectTheAllCategoriesLink();
    }

    @And("^I move mouse over the station logo")
    public void iMoveMouseOverTheStationLogo(){
        new RadioHomePage(driver).iMoveMouseOverTheStationLogo();
    }

    @Then("^the ([^\"]*) drawer is closed$")
    public void theSectionDrawerIsClosed(String section){
        new RadioHomePage(driver).theSectionDrawerIsClosed(section.toLowerCase());
    }

    @Then("^the ([^\"]*) drawer is open and the other drawers are closed$")
    public void theSectionDrawerIsOpenAndTheOtherDrawersAreClosed(String section){
        new RadioHomePage(driver).theSectionDrawerIsOpenAndTheOtherDrawersAreClosed(section.toLowerCase());
    }

    @Then("^I can see the following categories$")
    public void iCanSeeTheFollowingCategories(List<String> categories){
        new RadioHomePage(driver).iCanSeeTheFollowingCategories(categories);
    }

    @Then("^the ([^\"]*) drawer is open$")
    public void theSectionDrawerIsOpen(String section){
        new RadioHomePage(driver).theSectionDrawerIsOpen(section.toLowerCase());
    }

    @Then("^I am on the all categories page$")
    public void iAmOnTheAllCategoriesPage(){
        new RadioHomePage(driver).iAmOnTheAllCategoriesPage();
    }

    @Then("^I can see that the content is switched$")
    public void iCanSeeThatTheContentIsSwitched(){
        new RadioHomePage(driver).iCanSeeThatTheContentIsSwitched();
    }


}
