package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kasbi on 17.07.2017.
 */
public class DriverFactory {
    protected static WebDriver driver;

    public DriverFactory(){
        PropertyReader propertyReader = new PropertyReader();

        if(driver == null){
            String browser = propertyReader.readProperty("browser");
            if (browser.equals("firefox")) {
                System.setProperty("webdriver.gecko.driver",
                        propertyReader.readProperty("firefox_driver_path"));
                driver = new FirefoxDriver();
            }else if(browser.equals("chrome")){
                System.setProperty("webdriver.chrome.driver",
                        propertyReader.readProperty("chrome_driver_path"));
//                Map<String, String> mobileEmulation = new HashMap<String, String>();
//                mobileEmulation.put("deviceName", "Nexus 6");
                Map<String, Object> chromeOptions = new HashMap<String, Object>();
//                chromeOptions.put("mobileEmulation", mobileEmulation);
                DesiredCapabilities dc = DesiredCapabilities.chrome();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("start-maximized");
                dc.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                driver = new ChromeDriver(dc);
            }
        }
    }

    public WebDriver getDriver(){
        return driver;
    }

    public void destroyDriver() {
        if(driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
