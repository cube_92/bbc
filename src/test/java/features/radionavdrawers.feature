Feature: Radio Nav Drawers

  AS A user

  I WANT to see more content associated with navigation links

  So THAT I can easily find what I am looking for

  Background:

    Given I am on the homepage http://www.bbc.co.uk/radio

#  Scenario Outline: Opening and closing the drawers
#
#    Given I can see the radio nav
#
#    When I select <section> in the radio nav
#
#    Then the <section> drawer is open
#
#    When I select <section> in the radio nav again
#
#    Then the <section> drawer is closed
#
#    Examples: of sections
#
#      | section |
#
#      | Stations |
#
#      | Categories |
#
#      | Schedules |
#
##  @todo
#
#  Scenario Outline: Opening a drawer should close the other drawers
#
#    Given I can see the radio nav
#
#    When I select <section> in the radio nav
#
#    Then the <section> drawer is open and the other drawers are closed
#
#    Examples: of sections
#
#      | section |
#
#      | Stations |
#
#      | Categories |
#
#      | Schedules |
#
##  @todo
#  Scenario: Selecting categories displays categories links
#
#    Given I can see the radio nav
#
#    When I select Categories in the radio nav
#
#    Then I can see the following categories
#
#      | Children’s |
#
#      | Entertainment |
#
#      | News |
#
#      | Comedy |
#
#      | Factual |
#
#      | Performances & Events |
#
#      | Documentaries |
#
#      | Learning |
#
#      | Religion & Ethics |
#
#      | Drama |
#
#      | Music |
#
#      | Sport |
#
##  @todo
#
#  Scenario: Selecting all categories navigates to all categories page
#
#    Given I can see the radio nav
#
#    When I select Categories in the radio nav
#
#    And I select the all categories link
#
#    Then I am on the all categories page

    #My scenarios
#  Scenario Outline: Possibility to open nations stations in Station drawer
#
#    Given I am on the radio home page
#
#    When I click stations link in the radio nav
#
#    And I click <station> logo from the list
#
#    Then I am redirected to local <station> network page as <station-link>
#
#    And on every page there is a list of
#
#    Examples: of stations
#
#    | station           | station-link                                 |
#    | radio1            | http://www.bbc.co.uk/radio/radio1            |
#    | 1xtra             | http://www.bbc.co.uk/radio/1xtra             |
#    | radio2            | http://www.bbc.co.uk/radio/radio2            |
#    | radio3            | http://www.bbc.co.uk/radio/radio3            |
#    | radio4            | http://www.bbc.co.uk/radio/radio4            |
#    | radio4extra       | http://www.bbc.co.uk/radio/radio4extra       |
#    | 5livesportsextra  | http://www.bbc.co.uk/radio/5livesportsextra  |
#    | 6music            | http://www.bbc.co.uk/radio/6music            |
#    | asiannetwork      | http://www.bbc.co.uk/radio/asiannetwork      |
#    | worldserviceradio | http://www.bbc.co.uk/radio/worldserviceradio |
#    | radioscotland     | http://www.bbc.co.uk/radio/radioscotland     |
#    | radionangaidheal  | http://www.bbc.co.uk/radio/radionangaidheal  |
#    | radioulster       | http://www.bbc.co.uk/radio/radioulser        |
#    | radiofoyle        | http://www.bbc.co.uk/radio/radiofoyle        |
#    | radiowales        | http://www.bbc.co.uk/radio/radiowales        |
#    | radiocymru        | http://www.bbc.co.uk/radio/radiocymru        |
#
#  Scenario: Selecting iPlayer Radio logo
#
#    Given I am on the radio home page
#
#    When I click the iPlayer Radio logo
#
#    Then I am redirected to Radio homepage http://www.bbc.co.uk/radio

#My own and tested scenarios

  Scenario: As a user I can see that content in Stations section
    is switched when mouse is over logo station


    When I can see the Stations section content

    And I move mouse over the station logo

    Then I can see that the content is switched



