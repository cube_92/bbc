package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by kasbi on 17.07.2017.
 */
public class RadioHomePage {
    private WebDriver driver;
    private final String[] sections = new String[]{"stations","categories","schedules"};
    private final static int TIMEOUT_LIMIT_IN_SECONDS = 10;

    @FindBy(linkText = "All Categories")
    private WebElement allCategories;

    @FindBy(className = "radionav")
    private WebElement radioNav;

    @FindBy(className = "section-header")
    private WebElement stationsSection;

    @FindBy(css = "a[class='service_logo_link'][href*='radio1']")
    private WebElement radioLogo;

    @FindBy(css = "li[class*='icon_wrap icon_wrap--svg radio1 current-service']")
    private WebElement hideServiceTitle;

    public RadioHomePage(WebDriver webDriver){
        this.driver = webDriver;
        PageFactory.initElements(driver, this);
    }

    public void iAmOnTheHomepage(String homepageAdress){
        (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS))
                .withMessage("Can not see the radio nav.")
                .until(ExpectedConditions.visibilityOf(radioNav));
//        System.out.println("Title: " + driver.getTitle() + " given: " + homepageAdress);
        Assert.assertTrue(driver.getCurrentUrl().contains(homepageAdress));
    }

    public void iCanSeeTheRadioNav(){
        (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS))
                .withMessage("Can not see the radio nav.")
                .until(ExpectedConditions.visibilityOf(radioNav));
    }

    public void iSelectSectionsInTheRadioNav (String section){
        driver.findElement(By.xpath("//*[@data-panelid='" + section + "']")).click();
    }

    public void theSectionDrawerIsOpen(String section){
        (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS))
                .withMessage("Section drawer is not open.")
                .until(ExpectedConditions.visibilityOfElementLocated(By.id(section + "-panel")));
        driver.findElement(By.id(section + "-panel")).isDisplayed();
   }

    public void iSelectSectionsInTheRadioNavAgain(String section){
        driver.findElement(By.xpath("//*[@data-panelid='" + section + "']")).click();
    }

    public void theSectionDrawerIsClosed(String section) {
        (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS))
                .withMessage("Section drawer is not close.")
                .until(ExpectedConditions.invisibilityOfElementLocated(By.id(section + "-panel")));
        Assert.assertFalse( isAnyElementVisible(section + "-panel"));
   }

    public void theSectionDrawerIsOpenAndTheOtherDrawersAreClosed(String section) {
        for (String tmpSection: sections) {
//            System.out.println(section + " equals " + tmpSection + ": " + tmpSection.equals(section));
            if (!tmpSection.equals(section)){
                (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS))
                        .withMessage("Can see the " + tmpSection + " drawer.")
                        .until(ExpectedConditions.invisibilityOfElementLocated(By.id(tmpSection + "-panel")));
//                Assert.assertFalse(isAnyElementVisible(tmpSection+"-panel"));
            }else{
                (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS))
                        .withMessage("Can not see the " + tmpSection + " drawer.")
                        .until(ExpectedConditions.visibilityOfElementLocated(By.id(tmpSection + "-panel")));
//                Assert.assertTrue(isAnyElementVisible(tmpSection+"-panel"));
            }
        }
    }

    public void iCanSeeTheFollowingCategories(List<String> categories){
        (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS))
                .until(ExpectedConditions.visibilityOfElementLocated(By.linkText(categories.get(0).replace("’" ,"'"))));
        for(String category: categories){
            driver.findElement(By.linkText(category.replace("’" ,"'"))).isDisplayed();
        }
    }

    public void iSelectTheAllCategoriesLink(){
        (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS))
                .withMessage("Can not see the all categories link.")
                .until(ExpectedConditions.visibilityOf(allCategories));
        allCategories.click();
    }

    public void iAmOnTheAllCategoriesPage(){
        (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS))
                .withMessage("Can not see the categories page")
                .until(ExpectedConditions.titleContains("Categories"));
    }

    public void iCanSeeTheStationsSectionContent(){
        (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS)).
                withMessage("Can not see the station section").
                until(ExpectedConditions.visibilityOf(stationsSection));
    }

    public void iMoveMouseOverTheStationLogo(){
//        System.out.println("Size: " + driver.findElements(By.
//                    cssSelector("span[class*='blq-hide service_title'][value='Radio 1']")).
//                    size());

        Actions builder = new Actions(driver);
        Action mouseOverRadioLogo = builder.moveToElement(radioLogo).build();
        mouseOverRadioLogo.perform();
    }

    public void iCanSeeThatTheContentIsSwitched(){
//        Wait for "1xtra" bo czasami nie dziala "radio1" ...
        (new WebDriverWait(driver, TIMEOUT_LIMIT_IN_SECONDS)).
                withMessage("Can not see the station section").
                until(ExpectedConditions.visibilityOfElementLocated(
                        By.cssSelector("a[class*='sch-item-time-link'][href*='1xtra']")));
    }


    private boolean isAnyElementVisible(String elementName){
        return driver.findElements(By.id(elementName)).size() > 0;
    }
}
